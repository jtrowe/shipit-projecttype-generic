use strict;
use warnings;

package ShipIt::ProjectType::Generic;


# ABSTRACT: Project type which can release any kind of code.


# COPYRIGHT


use base 'ShipIt::ProjectType';

use Cwd;
use Data::Printer;
use ShipIt::Util qw( slurp tempdir_obj write_file );
use YAML qw( Dump Load );


my $data_filename = 'shipit.yml';


=head2 new

Factory when called directly.
Returns undef if there is no data file.

=cut

sub new {
    my ($class) = @_;
    return undef unless -e $data_filename;
    my $self = $class->SUPER::new;
    return $self;
}


=head2 current_version

Returns the current version of the project using the data file.

=cut

sub current_version {
    my ($self) = @_;
    my $conf = $self->load_data;
    die "No version found" unless
        $conf->{version};
    $self->{conf} = $conf;
    $self->{projname}  = $conf->{name};
    $self->{projver}   = $conf->{version};
    return $self->{projver};
}


=head2 disttest

Tests the distribution.

=cut

sub disttest {
    my ($self) = @_;
    my $distfile = $self->_build_tempdist;

    # debug,
    my $size = -s $distfile;
    warn "distfile = $distfile (size = $size)\n";

    my ($basename) = $distfile =~ m!([^/]+)$! or die;

    my $testdir_o = tempdir_obj();
    my $testdir   = $testdir_o->directory;
    File::Copy::copy($distfile, $testdir)
        or die "Copy from $distfile to $testdir failed: $!\n";

    my $old_cwd = getcwd;
    warn "Changing to tempdir $testdir for make/make test...\n";
    chdir($testdir) or die "chdir to testdir $testdir failed";
    system("tar -zxvf $basename") and die "untar failed";

    my ($untardir) = $basename =~ m!^(.+)\.tar\.gz$! or die;
    die "Expected untarred dir $untardir, but not there" unless -d $untardir;
    chdir($untardir) or die;

    #print STDERR "disttest: conf:\n" . np($self->{conf}) . "\n";
    my $step_name = 'DistTest';
    my $step_info = $self->{conf}->{steps}->{$step_name};

    my @step_actions = qw( build test );
    foreach my $step_action ( @step_actions ) {
        foreach my $c ( @{ $step_info->{actions}->{$step_action} // [] } ) {
            my $cmd = join ' ', @{ $c };
            system($cmd) and die sprintf(
                    "command '%s' failed during action '%s' of step '%s'",
                    $cmd, $step_action, $step_name);
        }
    }

    # restore old working directory
    chdir($old_cwd) or die;

    return 1;
}


=head2 load_data

Loads project information from the data file.

=cut

sub load_data {
    my $self = shift;

    my $str = slurp($data_filename);
    my $conf = Load($str);

    return $conf;
}


=head2 makedist

Creates the distribution archive.

=cut

sub makedist {
    my $self = shift;
    my $distfile = $self->_build_tempdist;
    return $distfile;
}


=head2 update_version

Updates the project version in the data file.

=cut

sub update_version {
    my ($self, $newver) = @_;
    my $conf = $self->load_data;
    $conf->{version} = $newver
        or die "update_version failed";
    $self->write_data($conf);
}


=head2 write_data

Writes project information to the data file.

=cut

sub write_data {
    my ($self, $conf) = @_;
    write_file($data_filename, Dump($conf));
}


=head2 _build_tempdist

Builds a temporary distribution file.

=cut

sub _build_tempdist {
    my $self = shift;

    # NB: Earlier steps can update the release information
    $self->current_version;

    my $projname = $self->{projname} or die "no projname found?";
    my $projver  = $self->{projver}  or die "no projver found?";
    my $distfile = "$projname-$projver.tar.gz";

    # did we already build it for a DistTest step earlier?
    if (my $tdir = $self->{dist_tempdirobj}) {
        my $file = $tdir->directory . "/$distfile";
        return $file if -e $file;
    }

    die "Distfile $distfile already exists" if -e $distfile;

    $ENV{'SHIPIT_DIST_NAME'} = $self->{projname};
    $ENV{'SHIPIT_DIST_VERSION'} = $self->{projver};

    print STDERR "disttest: conf:\n" . np($self->{conf}) . "\n";
    my $step_name = 'DistTest';
    my $step_info = $self->{conf}->{steps}->{$step_name};

    my @step_actions = qw( distclean makedist );
    foreach my $step_action ( @step_actions ) {
        foreach my $c ( @{ $step_info->{actions}->{$step_action} // [] } ) {
            my $cmd = join ' ', @{ $c };
            system($cmd) and die sprintf(
                    "command '%s' failed during action '%s' of step '%s'",
                    $cmd, $step_action, $step_name);
        }
    }

    die "Distfile $distfile doesn't exist" unless -e $distfile;

    # throw object in $self, so it doesn't get rmtree'd until
    # $self goes out of scope, later.
    my $tdir = $self->{dist_tempdirobj} = tempdir_obj();

    File::Copy::move($distfile, $tdir->directory) or
        die "Failed to move dist $distfile to tempdir";

    return $tdir->directory . "/$distfile";
}

1;
